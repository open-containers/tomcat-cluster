ARG BASE_IMAGE_NAME=tomcat
ARG BASE_IMAGE_TAG=9
FROM ${BASE_IMAGE_NAME}:${BASE_IMAGE_TAG}
ARG BASE_IMAGE_TAG

ADD conf/server.xml /usr/local/tomcat/conf/
ADD conf/context.xml /usr/local/tomcat/conf/



# Optional: Add Prometheus agent for JMX monitoring
RUN mkdir /opt/prometheus && wget https://repo.maven.apache.org/maven2/io/prometheus/jmx/jmx_prometheus_javaagent/0.17.2/jmx_prometheus_javaagent-0.17.2.jar -O /opt/prometheus/prometheus.jar
ADD conf/prometheus.yaml /usr/local/tomcat/conf/
ARG prometheusport=9404
ENV JAVA_OPTS="-javaagent:/opt/prometheus/prometheus.jar=$prometheusport:conf/prometheus.yaml ${JAVA_OPTS}"
EXPOSE $prometheusport
